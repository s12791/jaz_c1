package servlets.tests;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.mockito.Mockito;

import servlets.InstallmentsCalculator;

public class TestInstallmentsCalculator extends Mockito{

	@Test
	public void servlet_should_not_show_results_to_user_when_the_loanAmount_is_null() throws IOException {
		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);
		PrintWriter writer = mock(PrintWriter.class);
		when(response.getWriter()).thenReturn(writer);
		InstallmentsCalculator servlet = new InstallmentsCalculator();
		when(request.getParameter("loanAmount")).thenReturn(null);
		servlet.doPost(request, response);
		verify(response).sendRedirect("/");
		}
	@Test
	public void servlet_should_not_show_results_to_user_if_the_loanAmount_is_empty() throws IOException {
		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);
		PrintWriter writer = mock(PrintWriter.class);
		when(response.getWriter()).thenReturn(writer);
		InstallmentsCalculator servlet = new InstallmentsCalculator();
		when(request.getParameter("loanAmount")).thenReturn("");
		servlet.doPost(request, response);
		verify(response).sendRedirect("/");
	}
	@Test
	public void servlet_should_not_show_results_to_user_when_the_installmentLoanNumber_is_null() throws IOException {
		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);
		PrintWriter writer = mock(PrintWriter.class);
		when(response.getWriter()).thenReturn(writer);
		InstallmentsCalculator servlet = new InstallmentsCalculator();
		when(request.getParameter("installmentLoanNumber")).thenReturn(null);
		servlet.doPost(request, response);
		verify(response).sendRedirect("/");
		}
	@Test
	public void servlet_should_not_show_results_to_user_if_the_installmentLoanNumber_is_empty() throws IOException {
		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);
		PrintWriter writer = mock(PrintWriter.class);
		when(response.getWriter()).thenReturn(writer);
		InstallmentsCalculator servlet = new InstallmentsCalculator();
		when(request.getParameter("installmentLoanNumber")).thenReturn("");
		servlet.doPost(request, response);
		verify(response).sendRedirect("/");
	}
	@Test
	public void servlet_should_not_show_results_to_user_when_the_fixedInterest_is_null() throws IOException {
		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);
		PrintWriter writer = mock(PrintWriter.class);
		when(response.getWriter()).thenReturn(writer);
		InstallmentsCalculator servlet = new InstallmentsCalculator();
		when(request.getParameter("fixedInterest")).thenReturn(null);
		servlet.doPost(request, response);
		verify(response).sendRedirect("/");
		}
	@Test
	public void servlet_should_not_show_results_to_user_if_the_fixedInterest_is_empty() throws IOException {
		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);
		PrintWriter writer = mock(PrintWriter.class);
		when(response.getWriter()).thenReturn(writer);
		InstallmentsCalculator servlet = new InstallmentsCalculator();
		when(request.getParameter("fixedInterest")).thenReturn("");
		servlet.doPost(request, response);
		verify(response).sendRedirect("/");
	}
	@Test
	public void servlet_should_not_show_results_to_user_when_the_fixedCharge_is_null() throws IOException {
		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);
		PrintWriter writer = mock(PrintWriter.class);
		when(response.getWriter()).thenReturn(writer);
		InstallmentsCalculator servlet = new InstallmentsCalculator();
		when(request.getParameter("fixedCharge")).thenReturn(null);
		servlet.doPost(request, response);
		verify(response).sendRedirect("/");
		}
	@Test
	public void servlet_should_not_show_results_to_user_if_the_fixedCharge_is_empty() throws IOException {
		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);
		PrintWriter writer = mock(PrintWriter.class);
		when(response.getWriter()).thenReturn(writer);
		InstallmentsCalculator servlet = new InstallmentsCalculator();
		when(request.getParameter("fixedCharge")).thenReturn("");
		servlet.doPost(request, response);
		verify(response).sendRedirect("/");
	}
	
}
