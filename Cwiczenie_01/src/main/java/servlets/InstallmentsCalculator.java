package servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/Calculator")
public class InstallmentsCalculator extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException{
		
		String loanAmount = request.getParameter("loanAmount");
		if(loanAmount==null || loanAmount.equals("")){
			response.sendRedirect("/");
		}
		response.setContentType("text/html");
		response.getWriter().println("<h1>Hello "+loanAmount+"</h1>");	
	}
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException{
		
		String loanAmount = request.getParameter("loanAmount");
		String installmentLoanNumber = request.getParameter("installmentLoanNumber");
		String fixedInterest = request.getParameter("fixedInterest");
		String fixedCharge = request.getParameter("fixedCharge");
		String decreasingFixed = request.getParameter("decreasingFixed");
		
		if(loanAmount==null || loanAmount.equals("") || installmentLoanNumber==null || installmentLoanNumber.equals("") || fixedInterest==null || fixedInterest.equals("") || fixedCharge==null || fixedCharge.equals("")){
			response.sendRedirect("/");
		}
		response.setContentType("text/html");
		response.getWriter().println("<table border='1'><tr><td>Installment number</td><td>Amount</td><td>Interest Amount</td><td>Fixed Charge</td><td>Total Installment Amount</td></tr>");

		//Pomocnicza wartość do pętli
		float countLoanAmount=(Float.parseFloat(loanAmount));
		float monthlyInterest=(1200+Float.parseFloat(fixedInterest))/1200;
		//Wzór na ratę kredytu o ratach stałych
		float fixedRate=(float) ((((countLoanAmount*(Math.pow(monthlyInterest,Float.parseFloat(installmentLoanNumber)))))*(monthlyInterest-1))/((Math.pow(monthlyInterest,Float.parseFloat(installmentLoanNumber))-1)));
		
		if (decreasingFixed.equals("fixed")){
			
			for(int i=1;i<=Integer.parseInt(installmentLoanNumber);i++){
				
			response.getWriter().println("<tr><td> "+
			i+
			"</td><td> "+
			((Float.parseFloat(loanAmount))/(Float.parseFloat(installmentLoanNumber)))+
			"</td><td> "+((fixedRate)-((Float.parseFloat(loanAmount))/(Float.parseFloat(installmentLoanNumber))))+
			"</td><td> "+Float.parseFloat(fixedCharge)+
			"</td><td> "+
			(fixedRate+Float.parseFloat(fixedCharge))+
			"</td></tr>");
			}
			response.getWriter().println("</table>");
		}
		else{
			for(int i=1;i<=Integer.parseInt(installmentLoanNumber);i++){
				
				response.getWriter().println("<tr><td> "+
				i+
				"</td><td> "+
				((Float.parseFloat(loanAmount))/(Float.parseFloat(installmentLoanNumber)))+
				"</td><td> "+(Float.parseFloat(fixedInterest)/100)*((countLoanAmount)/12)+
				"</td><td> "+Float.parseFloat(fixedCharge)+
				"</td><td> "+
				(((Float.parseFloat(loanAmount))/Float.parseFloat(installmentLoanNumber))+(Float.parseFloat(fixedInterest)/100)*((countLoanAmount)/12)+Float.parseFloat(fixedCharge))+
				"</td></tr>");
				
				countLoanAmount=countLoanAmount-((Float.parseFloat(loanAmount))/(Float.parseFloat(installmentLoanNumber)));
				}
				response.getWriter().println("</table>");
		}
	}
}
